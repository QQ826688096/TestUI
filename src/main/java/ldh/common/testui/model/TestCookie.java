package ldh.common.testui.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ldh on 2019/12/2.
 */
@Data
@NoArgsConstructor
public class TestCookie {

    private Integer id;
    private String name;
    private String value;
    private String domain;
    private String path;
    private String expires;
    private boolean secure;
    private boolean select = true;

    private boolean isNew = false;
    private SimpleBooleanProperty enable = new SimpleBooleanProperty(true);

    public TestCookie(String name, String value, String domain, String path, String expires, boolean secure) {
        this.name = name;
        this.value = value;
        this.domain = domain;
        this.path = path;
        this.expires = expires;
        this.secure = secure;
    }

    public void setSelect(boolean select) {
        this.select = select;
        setEnable(select);
    }

    public boolean getSelect() {
        return enableProperty().get();
    }

    public String toString() {
        String str = "name=%s;value=%s;domain=%s;path=%s;expires=%s;secure=%s;";
        return String.format(str, name, value, domain, path, expires, secure);
    }

    public boolean getEnable() {
        return enableProperty().get();
    }

    public void setEnable(Boolean isEnable) {
        enableProperty().set(isEnable);
    }

    public BooleanProperty enableProperty() {
        if (enable == null) {
            enable = new SimpleBooleanProperty(select);
        }
        if (isNew) {
            enable.set(select);
            isNew = false;
        }
        return enable;
    }
}
