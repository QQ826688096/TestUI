package ldh.common.testui.util;

import com.jfoenix.controls.base.IFXValidatableControl;
import com.jfoenix.validation.base.ValidatorBase;
import javafx.scene.Node;
import javafx.scene.control.Control;

/**
 * Created by ldh on 2019/12/2.
 */
public class JFoenixUtil {

    public static <T extends ValidatorBase> T  createValidatorBase(Class<T> clazz, IFXValidatableControl control) {
        T t = null;
        try {
            t = clazz.newInstance();
            return createValidatorBase(t, control);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    public static <T extends ValidatorBase> T  createValidatorBase(T t, IFXValidatableControl control) {
        t.setSrcControl((Node)control);
        control.getValidators().add(t);
        return t;
    }
}
